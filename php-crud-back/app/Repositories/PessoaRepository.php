<?php

namespace App\Repositories;

class PessoaRepository extends Repository {

    public function getModel() {
        return 'App\Pessoa';
    }

    public function getPrimaryKeyName() {
        return 'id_pessoa';
    }

    public function getValidationRules(array $data) {
        $rules = [
            'nome'            => 'required|string|max:255',
            'email'           => 'required|string|max:100|email',
            'rg'              => 'required|string|max:30',
            'endereco'        => 'required|string|max:255',
            'sexo'            => 'required|string|max:1',
            'data_nascimento' => 'required|date'
        ];

        if (!empty($data['id_pessoa'])) {
            return array_merge($rules, [
                'senha' => 'nullable|string|max:100'
            ]);
        }
        return array_merge($rules, [
            'senha' => 'required|string|max:100'
        ]);
    }

    public function transform(array $attributes) {
        $attributes = collect($attributes)->filter(function ($item) {
            return $item !== null && $item !== '' && $item !== false;
        })->toArray();

        if (!empty($attributes['senha'])) {
            $attributes['senha'] = sha1(md5($attributes['senha']));
        }

        return parent::transform($attributes);
    }

}