<?php

namespace App\Providers;

use App\Classes\JsonValidator;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class JsonValidatorServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('JsonValidator', function () {
            return new JsonValidator();
        });
    }
}
