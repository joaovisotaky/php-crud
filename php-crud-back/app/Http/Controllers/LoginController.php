<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Tymon\JWTAuth\Facades\JWTAuth;

class LoginController extends Controller
{
    public function login() {
        return view('admin.login');
    }

    public function painel() {
        return view('admin.painel');
    }

    public function autenticar(Request $request) {
        $token = $this->gerarToken($request->all());
        $user = Auth::user();

        return response()->json([
            'id_pessoa' => $user->id_pessoa,
            'email' => $user->email,
            'nome' => $user->nome,
            'token' => $token
        ]);
    }

    private function gerarToken($credenciais) {
        if (!$token = JWTAuth::attempt($credenciais)) {
            throw new AccessDeniedHttpException('Credenciais inválidas');
        }

        return $token;
    }

    public function sair() {
        Auth::logout();
    }
}
