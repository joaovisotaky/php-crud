<?php

namespace App\Classes;

use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;

class JsonValidator {
    public static function validate(array $data, array $rules) {
        $validator = Validator::make($data, $rules);
        $response = new Response();

        if ($validator->fails()) {
            throw new ValidationException($validator, $response);
        }
    }
}