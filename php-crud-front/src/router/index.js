import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Inicio from '@/components/Inicio'
import Pessoa from '@/components/Pessoa'
import Relatorio from '@/components/Relatorio'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Login',
            component: Login
        },
        {
            path: '/inicio',
            name: 'Inicio',
            component: Inicio
        },
        {
            path: '/pessoa',
            name: 'Pessoa',
            component: Pessoa
        },
        {
            path: '/relatorio',
            name: 'Relatorio',
            component: Relatorio
        }
    ]
})
