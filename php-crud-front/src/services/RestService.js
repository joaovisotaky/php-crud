import axios from 'axios'
import router from '../router'

export default class RestService {
    http = null

    constructor(url, intercept) {
        if (typeof intercept === 'undefined') {
            intercept = true;
        }

        this.http = axios.create({
            baseURL: url
        });

        const token = localStorage.getItem('token');
        if (token) {
            this.http.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token')
        }

        if (intercept) {
            this.http.interceptors.response.use(function (response) {
                return response
            }, function (error) {
                // Do something with response error
                if ([401, 403].indexOf(parseInt(error.response.status, 10)) >= 0) {
                    localStorage.removeItem('token')
                    router.push('/')
                }
                return Promise.reject(error)
            })
        }
    }

    get(url, params) {
        return this.http.get(url, params);
    }

    post(url, params) {
        return this.http.post(url, params);
    }

    put(url, params) {
        return this.http.put(url, params);
    }

    delete(url, params) {
        return this.http.delete(url, params);
    }
}