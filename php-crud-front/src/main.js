// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import VeeValidate, { Validator } from 'vee-validate'
import pt_BR from 'vee-validate/dist/locale/pt_BR'
import axios from 'axios';
import constants from './constants/constants'
import VueTheMask from 'vue-the-mask'
import moment from 'moment'

Validator.localize('pt_BR', pt_BR)
Vue.use(VueMaterial)
Vue.use(VeeValidate)
Vue.use(VueTheMask)

Vue.prototype.$moment = moment

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    constants,
    components: { App },
    template: '<App/>'
})
